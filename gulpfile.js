// Se importan las librerias
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');

// funcion css con estructura gulp
function css () {
    return gulp
        .src('scss/app.scss')
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(autoprefixer({
            overrideBrowserslist : ['last 2 versions'],
            cascade: false
        }) )
        .pipe(gulp.dest('css'))
}

//Se crea Tarea css
gulp.task('css',css);

// Se crea tarea por defecto con gulp en paralelo para qure genere tareas asincronicas
gulp.task('default', gulp.parallel( ()=> {
    gulp.watch('scss/*.scss',css)
    gulp.watch('index.html')
    })
);