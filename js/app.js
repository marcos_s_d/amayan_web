var slideIndex = 1;

window.onscroll = ()=>{scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
    upButton.style.display = "block";
  } else {
    upButton.style.display = "none";
  }
}

const upButton = document.querySelector(".upArrowButton");
upButton.addEventListener('click', ()=>{
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
})

function editClass(classSelect,addClass,removeClass){
    const elementSelected = document.querySelector(classSelect);
    elementSelected.classList.remove(removeClass);
    elementSelected.classList.add(addClass);
}

function openModal() {
    editClass('#modalGallery','displayBlock','displayNone');
}   

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function showSlides() {
    let slides = document.querySelectorAll(".slideGallery");
    if (slideIndex > slides.length) {slideIndex = 1;}
    if (slideIndex < 1) {slideIndex = slides.length;}
     for (let i = 0; i < slides.length; i++) {
        slides[i].classList.remove("displayBlock");
        slides[i].classList.add("displayNone");
    }
    slides[slideIndex-1].classList.remove("displayNone");
    slides[slideIndex-1].classList.add("displayBlock");
}

const images = document.querySelector(".product_gallery");
images.addEventListener('click', (event)=>{
    let classes=event.target.classList
    console.log(event.target.classList)
    if(classes.contains('imgGallery')||classes.contains('overImageSelect')) {
        slideIndex = parseInt(event.target.attributes.number_img.value);
        openModal();
        showSlides();
    }
});

const arrowsModal = document.querySelector(".galleryContent");
arrowsModal.addEventListener('click',(event)=>{
    if (event.target.nodeName === "A"){
        let operator = event.target.attributes.operator.value;
        plusSlides(parseInt(operator));
    }
});

const buttonClose = document.querySelector(".closeModal")
buttonClose.addEventListener("click", () =>{
    editClass('#modalGallery','displayNone','displayBlock');
});